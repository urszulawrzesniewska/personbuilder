package pl.codementors.wzorce;

public class Application {
    final WriterFactory writerFactory;
    final PersonProvider personProvider;

    public Application(WriterFactory writerFactory, PersonProvider personProvider) {
        this.writerFactory = writerFactory;
        this.personProvider = personProvider;
    }

    public void start(){
        final PersonWriter personWriter = writerFactory.get();
        personWriter.write(personProvider.getNewPerson());

    }
}
