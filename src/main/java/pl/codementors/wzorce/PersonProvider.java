package pl.codementors.wzorce;

public final class PersonProvider {

    public Person getNewPerson() {
        return new PersonBuilder()
        .withName("Urszula")
        .withStreet("Gdanska")
        .withHomeNumber(13)
        .build();
    }
}