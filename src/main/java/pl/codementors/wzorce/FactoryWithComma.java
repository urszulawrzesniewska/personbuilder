package pl.codementors.wzorce;

public class FactoryWithComma implements WriterFactory {
    @Override
    public PersonWriter get() {
        return new PersonWithComma();
    }
}
