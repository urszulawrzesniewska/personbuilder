package pl.codementors.wzorce;

public interface WriterFactory {
    PersonWriter get();
}
