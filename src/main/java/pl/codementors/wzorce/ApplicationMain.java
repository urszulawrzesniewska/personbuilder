package pl.codementors.wzorce;
import java.util.Scanner;

public class ApplicationMain {
    public static void main(String[] args) {
        System.out.println("Choose method: ");
        Scanner scanner = new Scanner(System.in);
        String choice = scanner.nextLine();
        WriterFactory writerFactory = null;
        if(choice.equals("Pipeline")) {
            writerFactory = new FactoryWithPipeline();
        } else if (choice.equals(("Comma"))) {
            writerFactory = new FactoryWithComma();
        }
        final PersonProvider personProvider = new PersonProvider();
        Application application = new Application(writerFactory,personProvider);
    application.start();
    }
}
