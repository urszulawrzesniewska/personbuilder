package pl.codementors.wzorce;

public class PersonBuilder {
    private String name;
    private String street;
    private int homeNumber;

    public PersonBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PersonBuilder withStreet (String street) {
        this.street = street;
        return this;
    }
    public PersonBuilder withHomeNumber (int homeNumber) {

        this.homeNumber = homeNumber;
        return this;
    }

    public Person build() {
        Person p = new Person();
        p.setName(this.name);
        Address address = new Address();
        address.setHomeNumber(this.homeNumber);
        address.setStreet(this.street);
        p.setAddress(address);
        return p;
    }
}
