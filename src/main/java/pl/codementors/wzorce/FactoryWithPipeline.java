package pl.codementors.wzorce;

public class FactoryWithPipeline implements WriterFactory {
    @Override
    public PersonWriter get() {
        return new PersonWithPipeline();
    }
}
