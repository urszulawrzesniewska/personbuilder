package pl.codementors.wzorce;

public interface PersonWriter {

    void write (Person person);
}
