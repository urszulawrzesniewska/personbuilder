package pl.codementors.wzorce;

public class PersonWithPipeline implements PersonWriter {

    @Override
    public void write(Person person) {
        System.out.println(person.getName()
                + "|" + person.getAddress().getStreet()
                + "|" + person.getAddress().getHomeNumber());

    }

}
